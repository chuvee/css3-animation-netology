Для того чтобы воспользоваться конфигурацией gulp в файле Gulpfile.js и запустить watcher, вам необходимо выполнить следующие команды:

1. Скачать и установить Node.js (https://nodejs.org/en/download/)
2. Зайти в директорию текущего проекта через терминал (Linux, Mac OS) или cmd (Windows) и выполнить установку зависимостей из package.json:
> npm install 
3. Установить gulp глобально (для запуска задач):
> npm install -g gulp
4. Теперь все готово для того чтобы запустить watcher 
> gulp styl:watch

Для того чтобы заработал livereload, нужно установить плагин для браузера: https://chrome.google.com/webstore/detail/livereload/jnihajbhpnppcggbcgedagnkighmdlei?hl=ru (Google Chrome)

Исчерпывающая статья про Timing Functions – https://habrahabr.ru/post/220715/